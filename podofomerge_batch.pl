#!/usr/bin/env perl

use strict;

opendir(DIR, $ARGV[0]);
my @files = grep { /\.pdf$/ } readdir(DIR);
close(DIR);

my $pdf1 = shift @files;
my $pdf2 = shift @files;
my $cli = sprintf "podofomerge %s %s VJ13_poster.pdf", $pdf1, $pdf2;
system($cli);

foreach (@files) {
	$cli = sprintf "podofomerge %s VJ13_poster.pdf VJ13_poster.pdf", $_;
	system($cli);
}
