#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

import scribus

class a1:

    def __init__(self, y_space, close_off):
        self.margins = 10
        self.page_dimensions = [594, 841]
        self.width = self.page_dimensions[0] - (2 * self.margins)
        self.height = self.page_dimensions[1] - (2 * self.margins)
        self.blob = 100 - y_space
        self.y_space_divided_by_two = y_space / 2
        self.height_minus_y_space = 100 - y_space
        self.height_minus_y_space_all_divided_by_two = self.height_minus_y_space / 2
        self.y_space = y_space
        self.close_off = close_off
        self.info = u"Jonctions/Verbindingen 13: Prototypes for transmission\nBrussels + on-line, 30 November - 4 December 2011\nhttp://vj13.constantvzw.org"

        self.instruction = "Text2"
        self.blob_11 = "Text3"
        self.blob_21 = "Text4"
        self.blob_12 = "Text5"
        self.blob_22 = "Text6"

        self.pdf = scribus.PDFfile()

    def percentage2y_position(self, percentage):
        y = (percentage / 100.0) * float(self.height)
        return 10 + y

    def percentage2height(self, percentage):
        y = (percentage / 100.0) * float(self.height)
        return y

    def scribus_quirks(self):
        # we work with millimeters
        scribus.setUnit(scribus.UNIT_MILLIMETERS)

        # The dictionary is a map fontfile -> scribus_font_name.
        # This works because interpolated font names are idiosyncratic.
        scribus_fonts = scribus.getXFontNames()
        self.font_dictionary = {}
        for font in scribus_fonts:
            self.font_dictionary[os.path.split(font[5])[1]] = font[0]

    def write_blob(self, point, frame):
        scribus.insertText(point["point"], -1, frame)
        scribus.selectText(0, 1, frame)
        scribus.deleteText(frame)
        my_zfill = str(point["weight"][1]).zfill(5)
        scribus.setFont(self.font_dictionary["UniversElse-" + point["weight"][0] + " to OSP_helvetica_serif X" + my_zfill + "-50.ttf"], frame)

    def write_instruction(self):
        text = {
                "en" : [u"You know how to whistle, don't you?",        u"Just put your lips together and blow!"],
                "fr" : [u"Vous savez sifflez, n'est-ce pas?",          u"Il vous suffit de joindre vos lèvres et de souffler!"],
                "nl" : [u"Je weet hoe je moet fluiten, of niet soms?", u"Zet gewoon je lippen op elkaar en blaas!"],
                }
        bsb = random.choice(text.keys())
        text2 = text[bsb][1]
        del text[bsb]
        my_choice = random.choice(text.keys())
        text1 = text[my_choice][0]
        scribus.setText(text1 + "\n" + text2, self.instruction)
        scribus.setFontSize(70, self.instruction)
        scribus.setLineSpacing(70, self.instruction)
        scribus.setFont(self.font_dictionary["UniversElse-Bold to OSP_helvetica_serif X00042-50.ttf"], self.instruction)

    def main_loop(self):
        my_points1 = [
            {"point" : u"e", "weight" : ["Regular", 19], "font_size" : 512},
            {"point" : u"u", "weight" : ["Regular", 15], "font_size" : 512},
            {"point" : u"o", "weight" : ["Regular", 19], "font_size" : 512},
            {"point" : u"o", "weight" : ["Regular", 31], "font_size" : 512},
            {"point" : u"e", "weight" : ["Light", 19], "font_size" : 512},
            {"point" : u"p", "weight" : ["Light", 19], "font_size" : 512},
            {"point" : u"U", "weight" : ["Bold", 19], "font_size" : 512},
            {"point" : u"u", "weight" : ["Bold", 15], "font_size" : 512},
            #            {"point" : u"o", "weight" : ["Bold", 23], "font_size" : 512},
            ]
    
        my_points2 = [
            {"point" : u"e", "weight" : ["Regular", 19], "font_size" : 512},
            {"point" : u"u", "weight" : ["Regular", 15], "font_size" : 512},
            {"point" : u"o", "weight" : ["Regular", 19], "font_size" : 512},
            {"point" : u"o", "weight" : ["Regular", 31], "font_size" : 512},
            {"point" : u"e", "weight" : ["Light", 19], "font_size" : 512},
            {"point" : u"p", "weight" : ["Light", 19], "font_size" : 512},
            {"point" : u"U", "weight" : ["Bold", 19], "font_size" : 512},
            {"point" : u"u", "weight" : ["Bold", 15], "font_size" : 512},
            #            {"point" : u"o", "weight" : ["Bold", 23], "font_size" : 512},
            ]

        product = []
        for page in my_points1:
            print page
            for point in my_points2:
                if page != point:
                    product.append([page, point])
                    print "\t" + str(point)
        random.shuffle(product)
        for i in range(50):
            page = product[i][0]
            point = product[i][1]
            # left
            self.write_blob(point, self.blob_11)
            my_point = {}
            my_point["point"] = point["point"]
            my_point["weight"] = [point["weight"][0], point["weight"][1] + 2]
            my_point["font_size"] = point["font_size"]
            self.write_blob(my_point, self.blob_12)
            # right
            self.write_blob(page, self.blob_21)
            my_page = {}
            my_page["point"] = page["point"]
            my_page["weight"] = [page["weight"][0], page["weight"][1] + 2]
            my_page["font_size"] = page["font_size"]
            self.write_blob(my_page, self.blob_22)
            
            self.write_instruction()
            
            self.pdf.file = str(i).zfill(2) + str(page["point"] + point["point"] + u".pdf")
            self.pdf.save()

if __name__ == "__main__":

    my_page = a1(20, "no")
    my_page.scribus_quirks()
    my_page.main_loop()
