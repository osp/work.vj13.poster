#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import pprint

import scribus

class a1:

    def __init__(self, y_space, close_off):
        self.margins = 10
        self.page_dimensions = [594, 841]
        self.width = self.page_dimensions[0] - (2 * self.margins)
        self.height = self.page_dimensions[1] - (2 * self.margins)
        self.blob = 100 - y_space
        self.y_space_divided_by_two = y_space / 2
        self.height_minus_y_space = 100 - y_space
        self.height_minus_y_space_all_divided_by_two = self.height_minus_y_space / 2
        self.y_space = y_space
        self.close_off = close_off
        self.info = u"Jonctions/Verbindingen 13: Prototypes for · pour · voor transmission\nBrussels + on-line, 30 November - 4 December 2011\nhttp://vj13.constantvzw.org"

    def percentage2y_position(self, percentage):
        y = (percentage / 100.0) * float(self.height)
        return 10 + y

    def percentage2height(self, percentage):
        y = (percentage / 100.0) * float(self.height)
        return y

    def scribus_quirks(self):
        # we work with millimeters
        scribus.setUnit(scribus.UNIT_MILLIMETERS)

        # The dictionary is a map fontfile -> scribus_font_name.
        # This works because interpolated font names are idiosyncratic.
        scribus_fonts = scribus.getXFontNames()
        self.font_dictionary = {}
        for font in scribus_fonts:
            self.font_dictionary[os.path.split(font[5])[1]] = font[0]

    def position_info(self):
        my_info = scribus.createText(self.margins, self.margins, self.width, self.percentage2height(self.y_space_divided_by_two))
        scribus.setText(self.info, my_info)
        scribus.setFontSize(35, my_info)
        scribus.setLineSpacing(35, my_info)
        scribus.setFont(self.font_dictionary["UniversElse-Bold to OSP_helvetica_serif X00042-50.ttf"], my_info)

    def instruction_frame(self):
        self.instruction_frame = scribus.createText(self.margins,
                self.percentage2y_position(self.y_space_divided_by_two*0.75), self.width, self.percentage2height(self.y_space_divided_by_two))

    def blob_row1(self):
        self.blob_11 = scribus.createText(self.margins, self.percentage2y_position(self.y_space), self.width, self.percentage2height(self.height_minus_y_space))
        self.blob_21 = scribus.createText(self.margins + (self.width/2), self.percentage2y_position(self.y_space), self.width, self.percentage2height(self.height_minus_y_space))

    def blob_row2(self):
        self.blob_12 = scribus.createText(self.margins, self.percentage2y_position(self.y_space + self.height_minus_y_space_all_divided_by_two), self.width, self.percentage2height(self.height_minus_y_space))
        self.blob_22 = scribus.createText(self.margins + (self.width/2), self.percentage2y_position(self.y_space + self.height_minus_y_space_all_divided_by_two), self.width, self.percentage2height(self.height_minus_y_space))

    def write_blob(self, point, frame):
        scribus.setText(point["point"], frame)
        my_zfill = str(point["weight"][1]).zfill(5)
        scribus.setFont(self.font_dictionary["UniversElse-" + point["weight"][0] + " to OSP_helvetica_serif X" + my_zfill + "-50.ttf"], frame)

    def main_loop(self):
        my_points1 = [
            {"point" : u"U", "weight" : ["Bold", 18], "font_size" : 512},
            {"point" : u"l", "weight" : ["Light", 24], "font_size" : 512},
            {"point" : u"u", "weight" : ["Regular", 18], "font_size" : 512},
            {"point" : u"e", "weight" : ["Bold", 21], "font_size" : 512},
            ]
    
        my_points2 = [
            {"point" : u"U", "weight" : ["Bold", 18], "font_size" : 512},
            {"point" : u"l", "weight" : ["Light", 24], "font_size" : 512},
            {"point" : u"u", "weight" : ["Regular", 18], "font_size" : 512},
            {"point" : u"e", "weight" : ["Bold", 21], "font_size" : 512},
            ]

        for page in my_points1:
            print page
            for point in my_points2:
                if page != point:
                    print "\t" + str(point)
                    # left
                    self.write_blob(point, self.blob_11)
                    point["weight"] = [point["weight"][0], point["weight"][1] + 1]
                    self.write_blob(point, self.blob_12)
                    # right
                    self.write_blob(page, self.blob_21)
                    page["weight"] = [page["weight"][0], page["weight"][1] + 1]
                    self.write_blob(page, self.blob_22)
#                    my_page.pdf()

if __name__ == "__main__":

    my_page = a1(20, "no")
    my_page.scribus_quirks()
    my_page.position_info()
    my_page.instruction_frame()
    my_page.blob_row1()
    my_page.blob_row2()
    my_page.main_loop()
